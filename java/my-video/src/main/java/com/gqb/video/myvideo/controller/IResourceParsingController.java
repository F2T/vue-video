package com.gqb.video.myvideo.controller;

import java.util.List;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.gqb.video.myvideo.contract.req.QueryResourceRequest;
import com.gqb.video.myvideo.contract.resp.QueryVideoBaseResp;
import com.guoqb.springbootbasic.entity.ResponseResult;

@RequestMapping("/video/parse")
public interface IResourceParsingController {

	@PostMapping("index")
	ResponseResult<List<QueryVideoBaseResp>> index(QueryResourceRequest request);
	
	
}

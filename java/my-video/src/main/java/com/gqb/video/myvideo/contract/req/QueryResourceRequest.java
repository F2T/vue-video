package com.gqb.video.myvideo.contract.req;

import java.io.Serializable;

import javax.validation.constraints.NotBlank;

import lombok.Data;
import lombok.ToString;

import org.hibernate.validator.constraints.Length;

@Data
@ToString
public class QueryResourceRequest implements Serializable {

	private static final long serialVersionUID = -7204194512637327138L;

	@NotBlank
	@Length(max = 30)
	public String wd;

}

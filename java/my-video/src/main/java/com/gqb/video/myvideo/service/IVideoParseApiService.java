package com.gqb.video.myvideo.service;

import com.gqb.video.myvideo.contract.resp.QueryVideoBaseResp;

import java.util.List;

public interface IVideoParseApiService {
	
	
	/**
	 * 八戒采集资源解析
	 * @param wd
	 * @return
	 */
	List<QueryVideoBaseResp> videoParseToBj(String wd);

}

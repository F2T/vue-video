package com.gqb.video.myvideo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.ServletComponentScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.EnableAspectJAutoProxy;

@SpringBootApplication
@ServletComponentScan
@EnableAspectJAutoProxy
@ComponentScan({ "com.gqb.video.myvideo", "com.guoqb.springbootbasic" })
public class MyVideoApplication {

	public static void main(String[] args) {
		SpringApplication.run(MyVideoApplication.class, args);
	}

}

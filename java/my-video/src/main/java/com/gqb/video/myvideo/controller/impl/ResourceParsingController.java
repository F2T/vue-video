package com.gqb.video.myvideo.controller.impl;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.gqb.video.myvideo.contract.req.QueryResourceRequest;
import com.gqb.video.myvideo.contract.resp.QueryVideoBaseResp;
import com.gqb.video.myvideo.controller.IResourceParsingController;
import com.gqb.video.myvideo.service.IVideoParseApiService;
import com.guoqb.springbootbasic.entity.Response;
import com.guoqb.springbootbasic.entity.ResponseResult;

@RestController
public class ResourceParsingController implements IResourceParsingController {

	@Autowired
	private IVideoParseApiService videoParseApiService;

	@Override
	public ResponseResult<List<QueryVideoBaseResp>> index(@RequestBody @Valid QueryResourceRequest request) {

		return Response.makeOKRsp(videoParseApiService.videoParseToBj(request.getWd()));
	}

}

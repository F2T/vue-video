package com.gqb.video.myvideo.contract.resp;

import java.io.Serializable;
import java.util.List;

import lombok.Data;

@Data
public class QueryVideoBaseResp implements Serializable {

	private static final long serialVersionUID = 6549298780177807154L;

	public String vodName;

	public String vodActor;

	public String vodContent;

	public String vodPic;

	public String vodArea;

	public String vodLanguage;

	public String vodYear;

	public String vodRemarks;

	public String listName;

	public List<VodUrls> vodList;

	@Data
	public static class VodUrls {

		public String name;

		public String vodUrl;
	}

}

import axios from 'axios'
import { MessageBox, Message } from 'element-ui'
import { getToken } from '@/utils/auth'
// import qs from 'qs'

// create an axios instance
const service = axios.create({
  // baseURL: process.env.VUE_APP_BASE_API, // url = base url + request url
  baseURL: 'http://localhost:8099',
  // withCredentials: true, // send cookies when cross-domain requests
  timeout: 5000, // request timeout
  // withCredentials: true
  // method: 'post',
  // transformRequest: [
  //   data => {
  //     // data 就是你post请求传的值
  //     // 一下主要是吧数据拼接成 类似get格式
  //     let params = ''
  //     for (var index in data) {
  //       params += index + '=' + data[index] + '&'
  //     }
  //     return params
  //   }],
  headers: {
    'Content-Type': 'application/json;charset=UTF-8' // 默认是application/json形式的，需要设置成现在这样
  }
})

// request interceptor
service.interceptors.request.use(
  config => {
    // do something before request is sent

    config.headers['token'] = getToken()
    return config
  },
  error => {
    // do something with request error
    console.log(error) // for debug
    return Promise.reject(error)
  }
)

// response interceptor
service.interceptors.response.use(
  /**
   * If you want to get http information such as headers or status
   * Please return  response => response
  */

  /**
   * Determine the request status by custom code
   * Here is just an example
   * You can also judge the status by HTTP Status Code
   */
  response => {
    const res = response
    const data = response.data
    // if the custom code is not 20000, it is judged as an error.
    if (res.status !== 200) {
      Message({
        message: res.msg || 'Error',
        type: 'error',
        duration: 5 * 1000
      })
      // 50008: Illegal token; 50012: Other clients logged in; 50014: Token expired;
      if (res.data.code === 904 || res.code === 50012 || res.code === 50014) {
        // to re-login
        MessageBox.confirm('You have been logged out, you can cancel to stay on this page, or log in again', 'Confirm logout', {
          confirmButtonText: 'Re-Login',
          cancelButtonText: 'Cancel',
          type: 'warning'
        }).then(() => {
          store.dispatch('user/resetToken').then(() => {
            location.reload()
          })
        })
      }
      return Promise.reject(new Error(res.msg || 'Error'))
    } else {
      if (data.code === 904) {
        store.dispatch('user/resetToken').then(() => {
          location.reload()
        })
      } else if (data.code === '501') {
        Message({
          message: data.msg,
          type: 'error',
          duration: 5 * 1000
        })
        this.$message.success(data.msg)
        return Promise.reject(new Error(res.msg || 'Error'))
      } else {
        return response.data
      }
    }
  },
  error => {
    console.log('err' + error) // for debug
    Message({
      message: error.message,
      type: 'error',
      duration: 5 * 1000
    })
    return Promise.reject(error)
  }
)

export default service

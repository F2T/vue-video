import request from '@/utils/request'

export function videoParse(data) {
  return request({
    url: '/video/parse/index',
    method: 'post',
    data
  })
}